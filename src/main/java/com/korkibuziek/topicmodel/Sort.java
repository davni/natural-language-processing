package com.korkibuziek.topicmodel;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Sort {

	/*
	 * This method takes a Map as input and returns a sorted List<Entry<String,
	 * Double>> parts of this code is from
	 * http://www.mkyong.com/java/how-to-sort-a-map-in-java/
	 */
	public static List<String> getSortedListWithKeys(HashMap<String, Double> map) {
		List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(
				map.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator<Object>() {
			@SuppressWarnings({ "unchecked" })
			public int compare(Object o1, Object o2) {
				return ((Comparable<Double>) ((Map.Entry<String, Double>) (o2))
						.getValue())
						.compareTo(((Map.Entry<String, Double>) (o1))
								.getValue());
			}
		});

		// put sorted list into map again
		// LinkedHashMap make sure order in which keys were inserted
		List<String> sortedList = new LinkedList<String>();
		for (Iterator<Entry<String, Double>> it = list.iterator(); it.hasNext();) {
			Entry<String, Double> entry = it.next();
			sortedList.add(entry.getKey());
		}

		return sortedList;
	}

	/*
	 * This method takes a Map as input and returns a sorted List<Entry<String,
	 * Double>> parts of this code is from
	 * http://www.mkyong.com/java/how-to-sort-a-map-in-java/
	 */
	public static List<Entry<String, Double>> getSortedListWithEntries(
			HashMap<String, Double> map) {
		List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(
				map.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator<Object>() {
			@SuppressWarnings({ "unchecked" })
			public int compare(Object o1, Object o2) {
				return ((Comparable<Double>) ((Map.Entry<String, Double>) (o2))
						.getValue())
						.compareTo(((Map.Entry<String, Double>) (o1))
								.getValue());
			}
		});

		return list;
	}
}