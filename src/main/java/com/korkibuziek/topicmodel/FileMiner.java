package com.korkibuziek.topicmodel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * This class extract the words from the corpus and convert them to Document and Word instances
 */
public class FileMiner {

	private File stopWords;
	// reference list to documents
	private List<File> documents;

	public FileMiner(String corpusFolderName, File stopWordsFile) {
		this.stopWords = stopWordsFile;
		this.documents = new ArrayList<File>();
		try {
			// Append document references
			setDocumentReferences(corpusFolderName);
			// Extract words from documents and populate Word and Document
			extractWords(documents);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public static void Extract(String corpusFolderName, File stopWordsFile) {
		new FileMiner(corpusFolderName, stopWordsFile);
	}
	
	public static void Extract(String corpusFolderName) {
		new FileMiner(corpusFolderName, null);
	}

	/*
	 * This method takes a path to a corpus folder as argument and the iterates
	 * through all the containing documents and adds each path to the global
	 * variable documents.
	 */
	private void setDocumentReferences(String corpusFolderName) {
		File[] files = new File(corpusFolderName).listFiles();
		for (File file : files) {
			// if directory call this method for that as well
			if (file.isDirectory()) {
				setDocumentReferences(file.toString());
			} else {
				// add document names to the list (but not dotfiles)
				if (!file.getName().startsWith(".")) {
					documents.add(file);
				}
			}
		}
	}

	/*
	 * This method iterates through a list of txt-files. For every encountered
	 * word/token it removes unwanted tokens and calls the "wordEncountered"
	 * method in the Word class. For every document the "documentEncountered"
	 * gets called upon in the Document class.
	 */
	private void extractWords(List<File> documents) throws IOException {
		// for each document read each line and clean it
		// TODO fix the normalization
		for (File document : documents) {
			double documentLength = 0;
			BufferedReader br = new BufferedReader(new FileReader(document));
			String line = br.readLine();

			while (line != null) {
				// to lowercase and remove stopwords
				line = line.toLowerCase();
				line = line
						.replaceAll(
								"\\.|\\,|\\?|\\!|\\;|\\:|\"|-|\\(|\\)|&|''|\\—|\\–|\\       |“|”",
								"");
				line = line.replace("’", "'");
				String[] tokens = line.split("\\s");
				tokens = removeStopWords(tokens);
				documentLength += tokens.length;
				for (String word : tokens)
					Word.wordEncountered(word, document.getName());
				line = br.readLine();
			}
			br.close();
			Document.documentEncountered(document.getName(), documentLength);
		}
	}

	/*
	 * Remove stopwords removes stopwords from a string array
	 */
	private String[] removeStopWords(String[] tokens) throws IOException {
		if (stopWords != null) {
			// initialize stop words
			BufferedReader br = new BufferedReader(new FileReader(stopWords));
			String line = br.readLine();
			Set<String> stopSet = new HashSet<String>();

			while (line != null) {
				stopSet.add(line);
				line = br.readLine();
			}

			br.close();

			// remove stop words from token array
			List<String> cleanedTokens = new ArrayList<String>();
			for (String token : tokens) {
				if (!stopSet.contains(token))
					cleanedTokens.add(token);
			}

			Object[] array = cleanedTokens.toArray();
			// return a string array
			return Arrays.copyOf(array, array.length, String[].class);
		} else {
			return tokens;
		}
	}
}
