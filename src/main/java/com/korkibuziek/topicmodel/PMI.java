package com.korkibuziek.topicmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class PMI {

	public PMI() {
	}

	/*
	 * This groups the elements in the input list (<String>) by PMI, where each
	 * element in the list is a word. The word must also be an instance of the
	 * Word class.
	 */
	public void groupByPMI(List<String> inputWords, int maxWords,
			double threshold) {
		HashMap<String, Word> words = Word.getWords();
		StringBuilder sb1 = new StringBuilder();
		for (int i = 0; i < maxWords; i++) {
			String w1 = inputWords.get(i);
			Word word1 = words.get(w1);
			StringBuilder sb2 = new StringBuilder();
			sb2.append(w1 + " ");
			for (int j = i + 1; j < maxWords; j++) {
				String w2 = inputWords.get(j);
				Word word2 = words.get(w2);
				double pmi = calculatePMI(word1, word2);
				if (pmi > threshold) {
					sb2.append(w2 + " ");
				}
			}
			String s = sb2.toString();
			if (s.split(" ").length > 1)
				sb1.append(sb2.toString() + "\n");
		}
		System.out.println(sb1.toString());
	}

	public void setTargetWords(List<String> inputWords, int maxWords,
			double threshold) {
		if (inputWords.size() < maxWords)
			maxWords = inputWords.size();
		System.err.println("Number of words in model: " + maxWords);
		HashMap<String, Word> words = Word.getWords();
		for (int i = 0; i < maxWords; i++) {
			String w1 = inputWords.get(i);
			Word word1 = words.get(w1);
			double greatestPMI = 0;
			int index = 0;
			for (int j = 0; j < maxWords; j++) {
				if (i != j) {
					String w2 = inputWords.get(j);
					Word word2 = words.get(w2);
					double pmi = calculatePMI(word1, word2);

					if (pmi > threshold && pmi > greatestPMI) {
						index = j;
						greatestPMI = pmi;
					}
				}
			}
			if (i != index) {
				String targetWord = inputWords.get(index);
				word1.setTargetWord(words.get(targetWord));
			}
		}
	}

	public ArrayList<ArrayList<Double>> generatePMIMatrix(
			List<String> inputWords, int maxWords, double threshold) {
		HashMap<String, Word> words = Word.getWords();
		ArrayList<ArrayList<Double>> matrix = new ArrayList<ArrayList<Double>>();
		for (int i = 0; i < maxWords; i++) {
			ArrayList<Double> pmiArray = new ArrayList<Double>();
			String w1 = inputWords.get(i);
			Word word1 = words.get(w1);
			for (int j = 0; j < maxWords; j++) {
				String w2 = inputWords.get(j);
				Word word2 = words.get(w2);
				double pmi = calculatePMI(word1, word2);
				if (pmi > threshold) {
					pmiArray.add(pmi);
				} else {
					pmiArray.add(0.0);
				}
			}
			matrix.add(pmiArray);
		}
		return matrix;
	}

	/*
	 * This method groups the words by each vector std
	 */
	public void groupByStd(List<String> inputWords,
			ArrayList<ArrayList<Double>> matrix, double threshold) {
		HashMap<String, Double> stdMap = new HashMap<String, Double>();
		for (int i = 0; i < matrix.size(); i++) {
			ArrayList<Double> currentArray = matrix.get(i);
			stdMap.put(inputWords.get(i), VectorAlgebra.std(currentArray));
		}
		double previousStd = 99;
		for (Entry<String, Double> e : Sort.getSortedListWithEntries(stdMap)) {
			double currentStd = e.getValue();
			if (previousStd - currentStd > threshold) {
				System.out.println("--------------------------");
			}
			System.out.println(e.getKey());
			previousStd = currentStd;
		}
	}

	public void getAllDistances(List<String> inputWords,
			ArrayList<ArrayList<Double>> matrix) {
		for (int i = 0; i < matrix.size(); i++) {
			ArrayList<Double> array1 = matrix.get(i);
			int mindex = 0;
			double minVal = 2;
			for (int j = i + 1; j < matrix.size(); j++) {
				ArrayList<Double> array2 = matrix.get(j);
				double dist = VectorAlgebra.cosineDistance(array1, array2);
				if (dist < minVal) {
					minVal = dist;
					mindex = j;
				}
			}
			System.out.println(inputWords.get(i) + ": "
					+ inputWords.get(mindex));
		}

	}

	/*
	 * log (d(x,y) / (d(x) * (d(y) / D) + (√(d(x)) * 0.23))). d(x,y) is the
	 * number of documents in which both x and y occur, d(x) is the number of
	 * documents in which x occurs, d(y) is the number of documents in which y
	 * occurs, D is the total number of documents, 0.23 is given by:
	 * √(log(delta) / -2) with delta=0.9 (which seems like a good, albeit
	 * arbitrary, choice)
	 */
	private double calculatePMI(Word word1, Word word2) {
		double dx = word1.getDocumentFrequency();
		double dy = word2.getDocumentFrequency();
		double dxy = word1.getMutualDocuments(word2);
		double D = Document.getDocumentCount();

		return Math.log(dxy / (dx * (dy / D) + (Math.sqrt(dx * 0.23))));
	}
}