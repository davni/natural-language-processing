package com.korkibuziek.topicmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Word {

	/*
	 * static fields
	 */
	// hashMap with all the words. The key is the String name and the value is a
	// word instance
	private static HashMap<String, Word> words = new HashMap<String, Word>();
	// hashMap with String word as key and its tf idf as value
	private static HashMap<String, Double> wordTfIdf = new HashMap<String, Double>();

	/*
	 * instance fields
	 */
	// document : term frequency
	private HashMap<String, Double> documents = new HashMap<String, Double>();
	private HashMap<String, Double> tfIdf = new HashMap<String, Double>();
	// target. Makes it possible to set a target word (ex : when clustering
	// words)
	private Word tagetWord = null;
	// word id
	private String wordId = "";

	private Word(String word, String doc) {
		this.setWordId(word);
		words.put(word, this);
		this.addDocument(doc);
	}

	/*
	 * This static class determines whether to create a new word instance or to
	 * modify if it already exists
	 */
	public static void wordEncountered(String word, String doc) {
		if (words.containsKey(word)) {
			modifyWord(word, doc);
		} else {
			new Word(word, doc);
		}
	}

	/*
	 * Returns the word - tfidf map
	 */
	public static HashMap<String, Double> getWordTfIdfMap() {
		return wordTfIdf;
	}

	/*
	 * this method increments the TF in a document if it exist. Otherwise it
	 * adds the new document
	 */
	private static void modifyWord(String word, String doc) {
		Word w = words.get(word);
		if (w.existsDocument(doc))
			w.incrementTfInDocument(doc);
		else
			w.addDocument(doc);
	}

	/*
	 * Returns the word HashMap
	 */
	public static HashMap<String, Word> getWords() {
		return words;
	}

	/*
	 * Returns top tfIdf
	 */
	public static HashMap<String, Double> getTopTfIdf() {
		HashMap<String, Double> map = new HashMap<String, Double>();
		for (Entry<String, Word> e : words.entrySet()) {
			Word w = e.getValue();
			double t = w.topTfIdf();
			if(t != 0.0)
				map.put(e.getKey(), t);
		}
		return map;
	}

	/*
	 * Resets the static fields
	 */
	public static void reset() {
		words.clear();
		wordTfIdf.clear();
	}

	private Double topTfIdf() {
		List<Entry<String, Double>> l = Sort.getSortedListWithEntries(this.tfIdf);
		try {
			return l.get(0).getValue();
		} catch (Exception e) {
			return 0.0;
		}
	}
	
	/*
	 * returns the tf-idf norm: sqrt(tfidf1² + tfIdf2² + ...)
	 */
	public double getTfIdfNorm() {
		double sum = 0.0;
		for(Entry<String, Double> e : tfIdf.entrySet()) {
			sum = sum + Math.pow(e.getValue(), 2);
		}
		return Math.sqrt(sum);
	}

	/*
	 * This method appends a tfIdf value
	 */
	public void addTfIdf(double tfIdf) {
		wordTfIdf.put(this.wordId, tfIdf);
	}

	/*
	 * This method appends a tfIdf value
	 */
	public void addTfIdf(String document, double tfIdf) {
		this.tfIdf.put(document, tfIdf);
	}

	public HashMap<String, Double> getDocTfIdf() {
		return this.tfIdf;
	}

	/*
	 * This method returns how many documents the word and queried word have in
	 * common
	 */
	public Double getMutualDocuments(Word word) {
		double mutualDocuments = 0;
		for (String s : documents.keySet()) {
			HashMap<String, Double> m = word.getDocuments();
			if (m.containsKey(s)) {
				mutualDocuments++;
			}
		}
		return mutualDocuments;
	}

	/*
	 * Returns document frequency for the word
	 */
	public Double getDocumentFrequency() {
		return (double) documents.size();
	}

	/*
	 * Returns the documents the word occur in
	 */
	public HashMap<String, Double> getDocuments() {
		return documents;
	}

	/*
	 * Returns whether a given document exists in the documents hashMap for a
	 * word
	 */
	private boolean existsDocument(String doc) {
		return documents.containsKey(doc) ? true : false;
	}

	/*
	 * Adds a document with term frequency 1 in that document (science it's the
	 * first time the word is encountered in that doc)
	 */
	private void addDocument(String doc) {
		documents.put(doc, 1.0);
	}

	/*
	 * This method increments the term frequency in a document with one
	 */
	private void incrementTfInDocument(String doc) {
		double value = documents.get(doc) + 1;
		documents.put(doc, value);
	}

	/*
	 * Adds target word
	 */
	public void setTargetWord(Word targetWord) {
		// System.out.println(this.getWordId() + ": " + targetWord.getWordId());
		this.tagetWord = targetWord;
	}

	/*
	 * Get target word
	 */
	public Word getTagetWord() {
		return this.tagetWord;
	}

	public String getWordId() {
		return wordId;
	}

	public void setWordId(String wordId) {
		this.wordId = wordId;
	}
}
