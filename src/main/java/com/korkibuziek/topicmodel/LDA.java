package com.korkibuziek.topicmodel;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.*;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicAssignment;
import cc.mallet.types.*;

public class LDA {

	Pipe pipe;

	public LDA() {
		pipe = buildPipe();
	}

	public Pipe buildPipe() {
		ArrayList pipeList = new ArrayList();

		// Read data from File objects
		pipeList.add(new Input2CharSequence("UTF-8"));

		// Regular expression for what constitutes a token.
		// This pattern includes Unicode letters, Unicode numbers,
		// and the underscore character. Alternatives:
		// "\\S+" (anything not whitespace)
		// "\\w+" ( A-Z, a-z, 0-9, _ )
		// "[\\p{L}\\p{N}_]+|[\\p{P}]+" (a group of only letters and numbers OR
		// a group of only punctuation marks)
		Pattern tokenPattern = Pattern.compile("[\\p{L}\\p{N}_]+");

		// Tokenize raw strings
		pipeList.add(new CharSequence2TokenSequence(tokenPattern));

		// Normalize all tokens to all lowercase
		pipeList.add(new TokenSequenceLowercase());

		// Remove stopwords from a standard English stoplist.
		// options: [case sensitive] [mark deletions]
		pipeList.add(new TokenSequenceRemoveStopwords(new File(
				"src/main/resources/stoplists/stoplista.txt"), "UTF-8", false,
				false, false));

		// Rather than storing tokens as strings, convert
		// them to integers by looking them up in an alphabet.
		pipeList.add(new TokenSequence2FeatureSequence());

		// Do the same thing for the "target" field:
		// convert a class label string to a Label object,
		// which has an index in a Label alphabet.
		pipeList.add(new Target2Label());

		// Now convert the sequence of features to a sparse vector,
		// mapping feature IDs to counts.
		// pipeList.add(new FeatureSequence2FeatureVector());

		// Print out the features and the label
		pipeList.add(new PrintInputAndTarget());

		return new SerialPipes(pipeList);
	}

	public InstanceList readDirectory(File directory) {
		return readDirectories(new File[] { directory });
	}

	public InstanceList readDirectories(File[] directories) {

		// Construct a file iterator, starting with the
		// specified directories, and recursing through subdirectories.
		// The second argument specifies a FileFilter to use to select
		// files within a directory.
		// The third argument is a Pattern that is applied to the
		// filename to produce a class label. In this case, I've
		// asked it to use the last directory name in the path.
		FileIterator iterator = new FileIterator(directories, new TxtFilter(),
				FileIterator.LAST_DIRECTORY);

		// Construct a new instance list, passing it the pipe
		// we want to use to process instances.
		InstanceList instances = new InstanceList(pipe);

		// Now process each instance provided by the iterator.
		instances.addThruPipe(iterator);

		return instances;
	}

	public static void main(String[] args) throws IOException {

		LDA importer = new LDA();
		InstanceList instances = importer.readDirectory(new File(
				"../lonewolf/src/main/resources/txt-resources"));

		int numTopics = 8;
		ParallelTopicModel topicModel = new ParallelTopicModel(numTopics);
		topicModel.addInstances(instances);

		// run the model
		topicModel.setNumIterations(1000);
		topicModel.estimate();
		
		topicModel.printTopWords(System.out, 10, false);

//		// Show the words and topics for all instances
//
//		// The data alphabet maps word IDs to strings
//		Alphabet dataAlphabet = instances.getDataAlphabet();
//		
//		for (int i = 0; i < instances.size(); i++) {
//			FeatureSequence tokens = (FeatureSequence) topicModel.getData()
//					.get(i).instance.getData();
//			LabelSequence topics = topicModel.getData().get(i).topicSequence;
//
//			Formatter out = new Formatter(new StringBuilder(), Locale.US);
//			for (int position = 0; position < tokens.getLength(); position++) {
//				out.format("%s-%d ", dataAlphabet.lookupObject(tokens
//						.getIndexAtPosition(position)), topics
//						.getIndexAtPosition(position));
//			}
//			System.out.println(out);

//			instances.save(new File(
//					"src/main/resources/mallet-output-files/test.mallet"));
//
//			// Estimate the topic distribution of the instance,
//			// given the current Gibbs state.
//			double[] topicDistribution = topicModel.getTopicProbabilities(i);
//
//			// Get an array of sorted sets of word ID/count pairs
//			ArrayList<TreeSet<IDSorter>> topicSortedWords = topicModel
//					.getSortedWords();
//
//			// Show top 5 words in topics with proportions for the document
//			for (int topic = 0; topic < numTopics; topic++) {
//				Iterator<IDSorter> iterator = topicSortedWords.get(topic)
//						.iterator();
//
//				out = new Formatter(new StringBuilder(), Locale.US);
//				out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
//				int rank = 0;
//				while (iterator.hasNext() && rank < 5) {
//					IDSorter idCountPair = iterator.next();
//					out.format("%s (%.0f) ",
//							dataAlphabet.lookupObject(idCountPair.getID()),
//							idCountPair.getWeight());
//					rank++;
//				}
//				System.out.println(out);
//			}
//		}
	}

	/** This class illustrates how to build a simple file filter */
	class TxtFilter implements FileFilter {

		/**
		 * Test whether the string representation of the file ends with the
		 * correct extension. Note that {@ref FileIterator} will only call this
		 * filter if the file is not a directory, so we do not need to test that
		 * it is a file.
		 */
		public boolean accept(File file) {
			return file.toString().endsWith(".txt");
		}
	}

}
