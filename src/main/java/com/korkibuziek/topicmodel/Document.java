package com.korkibuziek.topicmodel;

import java.util.HashMap;

public class Document {

	/*
	 * static fields
	 */
	private static HashMap<String, Document> documents = new HashMap<String, Document>();
	private static HashMap<String, Double> documentLengths = new HashMap<String, Double>();
	private static HashMap<String, Double> documentTFIDF = new HashMap<String, Double>();
	
	/*
	 * instance fields
	 */
	private String docId = "";

	private Document(String doc, Double length) {
		this.docId = doc;
		documents.put(doc, this);
		documentLengths.put(doc, length);
	}

	public static void documentEncountered(String doc, Double length) {
		if (documents.containsKey(doc)) {
			Document document = documents.get(doc);
			document.incrementDocumentLength(doc, length);
		} else {
			new Document(doc, length);
		}
	}

	public static Double getDocumentLength(String doc) {
		return documentLengths.get(doc);
	}
	
	public static Double getDocumentCount() {
		return (double) documents.size();
	}
	
	public static HashMap<String,Double> getDocumetTFIDFMap() {
		return documentTFIDF;
	}

	/*
	 * appends tf*idf - scores to the documnet tf*idf map
	 */
	public static void putTFIDF(String doc, Double TFIDF) {
		if (documentTFIDF.containsKey(doc)) {
			double value = documentTFIDF.get(doc) + TFIDF;
			documentTFIDF.put(doc, value);
		} else {
			documentTFIDF.put(doc, TFIDF);
		}

	}
	
	public String getDocumentId() {
		return this.docId;
	}

	private void incrementDocumentLength(String doc, Double length) {
		double value = documentLengths.get(doc) + length;
		documentLengths.put(doc, value);
	}

}
