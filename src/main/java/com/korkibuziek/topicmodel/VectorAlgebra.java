package com.korkibuziek.topicmodel;

import java.util.ArrayList;

public class VectorAlgebra {
	
	public static Double cosineDistance(ArrayList<Double> array1,
			ArrayList<Double> array2) {
		return 1 - (dot(array1, array2) / (norm(array1) * norm(array2)));
	}

	/*
	 * This method returns the L2-norm of the scores.
	 */
	public static double norm(ArrayList<Double> array) {
		double sum = 0;
		for (double d : array) {
			sum = (sum + (d * d));
		}
		return (double) Math.sqrt(sum);
	}

	public static Double dot(ArrayList<Double> array1, ArrayList<Double> array2) {
		double dot = 0;
		for (int i = 0; i < array1.size(); i++) {
			dot = dot + array1.get(i) * array2.get(i);
		}
		return dot;
	}

	/*
	 * Mean
	 */
	public static double mean(ArrayList<Double> array) {
		double rowSum = 0;
		int length = array.size();
		for (double d : array) {
			rowSum += d;
		}
		return rowSum / length;
	}

	/*
	 * Standard deviation
	 */
	public static double std(ArrayList<Double> array) {
		double mean = mean(array);
		double sum = 0;
		for (double d : array) {
			sum = sum + Math.pow((d - mean), 2);
		}
		return array.size() > 1 ? Math.sqrt((sum) / (array.size() - 1)) : 0;
	}

}