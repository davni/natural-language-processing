package com.korkibuziek.topicmodel;

import java.util.HashMap;
import java.util.Map.Entry;

public class TFIDF {

	/*
	 * This method calculates the total tf idf for each document. Mening that
	 * each document's tf idf is the sum of all its containing words tf idf.
	 */
	public static void calculateDocumentTfIdf(boolean normalize, boolean print) {
		// get words from the word class and iterate through them in order to
		// obtain @ which document they were encountered
		HashMap<String, Word> wordMap = Word.getWords();
		for (Entry<String, Word> wordEntry : wordMap.entrySet()) {
			Word word = wordEntry.getValue();
			// get document map with associated scores and extract all values in
			// order to call the calculateTFIDF method and later putTFIDF
			HashMap<String, Double> tfMap = word.getDocuments();
			for (Entry<String, Double> docEntry : tfMap.entrySet()) {
				String doc = docEntry.getKey();
				Double termFreq = docEntry.getValue();
				Double docLength = normalize == true ? Document
						.getDocumentLength(doc) : 1;
				Double docFreq = word.getDocumentFrequency();
				Double TFIDF = calculateTFIDF(termFreq, docFreq, docLength);
				Document.putTFIDF(doc, TFIDF);
			}
		}
		if (print)
			printDocumentTfIdf();
	}

	private static void printDocumentTfIdf() {
		HashMap<String, Double> tfidfMap = Document.getDocumetTFIDFMap();
		for (Entry<String, Double> e : tfidfMap.entrySet()) {
			String document = e.getKey();
			Double tfIdf = e.getValue();
			System.out.println(document + ", " + tfIdf);
		}
	}

	/*
	 * This method calculates the total tf idf for each document. Mening that
	 * each document's tf idf is the sum of all its containing words tf idf.
	 */
	public static void calculateWordTfIdf(boolean normalize,
			boolean removeLowFreq, boolean print) {
		// get words from the word class and iterate through them in order to
		// obtain @ which document they were encountered
		HashMap<String, Word> wordMap = Word.getWords();
		for (Entry<String, Word> wordEntry : wordMap.entrySet()) {
			Word word = wordEntry.getValue();
			// get document map with associated scores and extract all values in
			// order to call the calculateTFIDF method and later putTFIDF
			HashMap<String, Double> tfMap = word.getDocuments();
			Double docFreq = word.getDocumentFrequency();
			
			if (removeLowFreq && docFreq <= 1) {
				continue;
			}
			// iterate through all documents in which the word has occurred.
			for (Entry<String, Double> docEntry : tfMap.entrySet()) {
				String doc = docEntry.getKey();
				Double termFreq = docEntry.getValue();
				Double docLength = normalize == true ? Document
						.getDocumentLength(doc) : 1;
				Double TFIDF = calculateTFIDF(termFreq, docFreq, docLength);
				word.addTfIdf(doc, TFIDF);
			}
		}
		if (print)
			printWordTfIdf();
	}

	private static void printWordTfIdf() {
		HashMap<String, Word> word = Word.getWords();
		// all the words
		for (Entry<String, Word> e : word.entrySet()) {
			Word wrd = e.getValue();
			System.out.println(wrd.getWordId() + ":");
			HashMap<String, Double> tfIdfMap = wrd.getDocTfIdf();
			// all its documents
			for (Entry<String, Double> tfIdfEntry : tfIdfMap.entrySet()) {
				String doc = tfIdfEntry.getKey();
				Double tfIdf = tfIdfEntry.getValue();
				System.out.println("\t" + doc + ": " + tfIdf);
			}
		}
	}

	/*
	 * This method calculates the tf idf of all words.
	 */
	public static void calculateTermTfIdf(boolean normalize,
			boolean removeLowFreq, boolean print) {
		HashMap<String, Word> words = Word.getWords();
		for (Entry<String, Word> wrdEntry : words.entrySet()) {
			Word word = wrdEntry.getValue();
			HashMap<String, Double> docs = word.getDocuments();

			double docFreq = word.getDocumentFrequency();
			if (removeLowFreq && docFreq <= 1) {
				continue;
			}

			double tfIdf = 0.0;
			for (Entry<String, Double> e : docs.entrySet()) {
				String doc = e.getKey();
				Double TF = e.getValue();
				double length = normalize == true ? Document
						.getDocumentLength(doc) : 1;
				double IDF = 1 + Math.log(length / docFreq);
				tfIdf = tfIdf + ((TF / length) * IDF);
			}

			word.addTfIdf(tfIdf);
		}
		if (print)
			printTermTfIdf();
	}

	private static void printTermTfIdf() {
		HashMap<String, Double> map = Word.getWordTfIdfMap();
		for (Entry<String, Double> e : map.entrySet()) {
			System.out.println(e.getKey() + ": " + e.getValue());
		}
	}

	/*
	 * Calculates the TFIDF.
	 */
	private static double calculateTFIDF(double termFreq, double docFreq,
			double docLength) {
		double IDF = 1 + Math.log(docLength / docFreq);
		return (termFreq / docLength) * IDF;
	}

}
